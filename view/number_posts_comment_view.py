from PyInquirer import prompt
from metiers.search_by_pertinence import SearchByPertinence
from view.abstract_view import AbstractView
import metiers.import_api as ia

class NumberPostsCommentView(AbstractView):
    def __init__(self, id_user, list_publi_period):
        self.id_user = id_user
        self.list_publi_period = list_publi_period
        self.__questions = [
            {
                'type': 'input',
                'name': 'nb_posts_coment',
                'message': 'Veuillez entrer le "X" '
            }
        ]

    def make_choice(self):
        answers = prompt(self.__questions)
        list_publication_final = SearchByPertinence(list_publication = self.list_publi_period).pertinence_comments(int(answers["nb_posts_coment"]))
        print(ia.ImportAPI().formatting(list_publication_final))
        from view.export_post_view import ExportPostView
        return ExportPostView(self.id_user, list_publication_final).make_choice()
from view.abstract_view import AbstractView
from PyInquirer import prompt
from metiers.export_report import ExportReport

class ReportPeriodView(AbstractView):
    def __init__(self, id_user):
        self.id_user = id_user
        self.__questions = [
            {
                'type': 'input',
                'name': 'choix_periode_rapport',
                'message': 'Veuillez entrer une période en jour(s) : '
            }
        ]

    def make_choice(self):
        answers = prompt(self.__questions)
        ExportReport(self.id_user, answers['choix_periode_rapport']).export_report()
        print("Allez voir le fichier rapport.csv")
        from view.operations_view import OperationsView
        return OperationsView(self.id_user).make_choice()


from PyInquirer import  prompt
from view.abstract_view import AbstractView
from metiers.connection import Connection

class RegisterView(AbstractView):
    def __init__(self) -> None:
        self.__questions = [
            
            {
                'type': 'input',
                'name': 'pseudo',
                'message': 'Choisissez un pseudo'
            },
            {
                'type': 'password',
                'name': 'password',
                'message': 'Choisissez un mot de passe'
            }
        ]



    def make_choice(self):
        answers = prompt(self.__questions)
        Connection().create_account(answers["pseudo"], Connection().hash_password(answers["pseudo"], answers["password"]))
        print(" ")
        print("Veuillez vous identifier : ")
        print(" ")
        from view.connection_view import ConnectionView
        return ConnectionView().make_choice()



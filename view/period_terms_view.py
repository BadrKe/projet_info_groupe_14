from PyInquirer import prompt
from view.abstract_view import AbstractView
from metiers.search_post import SearchPost
from metiers.import_api import ImportAPI

class PeriodTermsView(AbstractView):
    def __init__(self, id_user):
        self.id_user = id_user
        self.__questions = [
            {
                'type': 'input',
                'name': 'choix_periode_terme',
                'message': 'Veuillez entrer une période en heure : '
            }
        ]


    def make_choice(self):
        answers = prompt(self.__questions)
        list_publi_period = SearchPost(answers["choix_periode_terme"], ImportAPI().get_api()).search_post_period()
        from view.search_terms_view import SearchTermsView
        return SearchTermsView(self.id_user, list_publi_period).make_choice()
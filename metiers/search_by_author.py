class SearchByAuthor:
    
    def __init__(self, list_publication):
        self.list_publication = list_publication

    def search_publication_author(self, pseudo):
        list_publication_final = []
        for publication in self.list_publication:
            if pseudo.upper() == (publication.author).upper():
                list_publication_final.append(publication)
        return list_publication_final

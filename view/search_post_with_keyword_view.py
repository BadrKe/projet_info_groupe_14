from PyInquirer import prompt
from metiers.search_by_word import SearchByWord
from view.abstract_view import AbstractView
import metiers.import_api as ia

class SearchPostWithKeywordView(AbstractView):
    def __init__(self, id_user, list_publi_period):
        self.id_user = id_user
        self.list_publi_period = list_publi_period
        self.__questions = [
            {
                'type': 'input',
                'name': 'choix_motcle',
                'message': 'Veuillez entrer un mot clé. Vous obtiendrez tous les posts contenant ce mot clé : '
            }
        ]


    def make_choice(self):
        answers = prompt(self.__questions)
        list_publication_final = SearchByWord(list_publication = self.list_publi_period).search_publication_with_word(answers["choix_motcle"])
        print(ia.ImportAPI().formatting(list_publication_final))
        from view.export_post_view import ExportPostView
        return ExportPostView(self.id_user, list_publication_final).make_choice()
import metiers.search_post as rp
import metiers.import_api as ia

class SearchByPertinence:
    '''
    Description :
    -----------

    Class with two methods. Each of them builds a sorted list of Publication. 
    Data used come from posts published on wallstreetbet appli on reddit posts platform (?)
    
    Returned tuples elements are in order of appearance :
    - title of the post
    - post diffusion date
    - reactions number
    - subreddit name

    The first of them sorts the collected publications depending on the number of comments associated with the given post it on reddit,
    number which does not appear here.
    The second of them sorts the collected publications depending on the number of reactions associated with the given post it on reddit,
    number defined as the difference between the number of positive reactions and negative ones. 
    

    Parameters :
    ------------
    list_publication : list of object Publication. This list only contains publications in the correct period.  

    '''    
    def __init__(self, list_publication):
        self.list_publication = list_publication
        

    def pertinence_comments(self, number_publication):

        '''
        Description:
        
        builds a sorted list of wallstreetsbet subreddit posts. 

        Parameters:
        -----------

        number_publication : integer
            the number of publication which will be part of the returned list.
        
        Returns :
        ---------

        A sorted list of object Publication. The number of element is equal to number_publication.
        Order of appearance is based upon the decreasing number of comments. 

        Examples 
        --------
        >>>>Publication1=Publication("Title1", "selftext1", 2021-11-09 22:00:14, "author1", nb_comments=12, nb_reactions=18, "wallstreetsbet")
        >>>>Publication2=Publication("Title2", "selftext2", 2021-11-10 22:00:14, "author2", nb_comments=10, nb_reactions=26, "wallstreetsbet")
        >>>>Publication3=Publication("Title3", "selftext3", 2021-11-10 22:00:14, "author3", nb_comments=25, nb_reactions=7, "wallstreetsbet")
        >>>>list_publication=[Publication1, Publication2, Publication3] 
        >>>>publi_list=SearchByPertinence(list_publication)
        >>>>publi_list.pertinence_comments(2)

        [Publication3, Publication1]

        '''

        list_tuples_nbcomments_publi = []
        list_publi_more_comment = []
        for publication in self.list_publication:
            list_tuples_nbcomments_publi.append((publication.nb_comments, publication))
        list_decreasing_order = sorted(list_tuples_nbcomments_publi, key=lambda x: x[0], reverse = True)
        list_tuples_publi_more_comment = list_decreasing_order[0:number_publication]
        for tuple in list_tuples_publi_more_comment:
            list_publi_more_comment.append(tuple[1])
        return list_publi_more_comment


    def pertinence_reaction(self, number_publication):
        
        '''
        Description:
        
        builds a sorted list of wallstreetsbet subreddit posts.

        Parameters:
        -----------

        number_publication : integer
            the number of publication which will be part of the returned list.
        
        Returns :
        ---------

        A sorted list of object Publication. The number of element is equal to number_publication.
        Order of appearance is based upon the decreasing number of reactions. 
    

        Examples 
        --------

        >>>>Publication1=Publication("Title1", "selftext1", 2021-11-09 22:00:14, "author1", nb_comments=12, nb_reactions=18, "wallstreetsbet")
        >>>>Publication2=Publication("Title2", "selftext2", 2021-11-10 22:00:14, "author2", nb_comments=10, nb_reactions=26, "wallstreetsbet")
        >>>>Publication3=Publication("Title3", "selftext3", 2021-11-10 22:00:14, "author3", nb_comments=25, nb_reactions=7, "wallstreetsbet")
        >>>>list_publication=[Publication1, Publication2, Publication3] 
        >>>>publi_list=SearchByPertinence(list_publication)
        >>>>publi_list.pertinence_reaction(2)

        [Publication2, Publication1]

        '''

        list_tuples_nbreaction_publi = []
        list_publi_with_more_reactions = []
        for publication in self.list_publication:
            list_tuples_nbreaction_publi.append((publication.nb_reactions, publication))
        list_decreasing_order = sorted(list_tuples_nbreaction_publi, key=lambda x: x[0], reverse = True)
        list_tuples_publi_with_more_reactions = list_decreasing_order[0:number_publication]
        for tuple in list_tuples_publi_with_more_reactions:
            list_publi_with_more_reactions.append(tuple[1])
        return list_publi_with_more_reactions





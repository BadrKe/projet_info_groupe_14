CREATE SEQUENCE seq_id_utilisateur;
CREATE TABLE utilisateur (
    id_utilisateur INT PRIMARY KEY DEFAULT nextval('seq_id_utilisateur'),
    pseudo_utilisateur VARCHAR(50),
    date_creation_utilisateur DATE,
    mot_de_passe VARCHAR(100));
	
CREATE SEQUENCE seq_id_action;
CREATE TABLE action_boursiere (
    id_action_boursiere INT PRIMARY KEY DEFAULT nextval('seq_id_action'),
    id_utilisateur INT,
    nom_action_boursiere VARCHAR(50),
    date_creation_dans_bdr DATE,
    occurrence INTEGER,
    CONSTRAINT id_utilisateur FOREIGN KEY (id_utilisateur) REFERENCES utilisateur);



CREATE SEQUENCE seq_id_post;
CREATE TABLE post (
    id_post INT PRIMARY KEY DEFAULT nextval('seq_id_post'),
    nom_auteur VARCHAR(100),
    titre VARCHAR(5000),
    date_post DATE,
    contenu VARCHAR(60000),
    nb_reactions INT,
    id_utilisateur INT,
    CONSTRAINT id_utilisateur FOREIGN KEY (id_utilisateur) REFERENCES utilisateur,
    nb_commentaires INT,
    date_ajout DATE,
    subreddit VARCHAR(150));


INSERT INTO utilisateur(pseudo_utilisateur,date_creation_utilisateur,mot_de_passe) VALUES('utilisateur1',current_date,'utilisateur1');

INSERT INTO action_boursiere(id_utilisateur, nom_action_boursiere, date_creation_dans_bdr, occurrence) VALUES
    (1,'DASSAULT',current_date,0),
    (1,'LVMH',current_date,0),
    (1,'SANOFI',current_date,0),
    (1,'TOTALENERGIES',current_date,0),
    (1,'FLEURY-MICHON',current_date,0),
    (1,'EUROTUNNEL',current_date,0);


INSERT INTO post(nom_auteur, titre, date_post, contenu, nb_reactions, id_utilisateur, nb_commentaires, date_ajout, subreddit) VALUES 
        ('auteur1', 'post1',current_date,'contenupost1',0, 1, 3, current_date, 'wallstreetbets'),
        ('auteur2','post2',current_date,'contenupost2',0, 1, 10, current_date, 'wallstreetbets'),
        ('auteur3','post3',current_date,'contenupost3',1, 1, 2, current_date, 'wallstreetbets'),
        ('auteur4','post4',current_date,'contenupost4',0, 1, 0, current_date, 'wallstreetbets');
from PyInquirer import prompt
from view.abstract_view import AbstractView
from dao.post_dao import PostDao

class ExportPostView(AbstractView):
    def __init__(self, id_user, list_publication):
        self.id_user = id_user
        self.list_publication = list_publication
        self.__questions = [
            {
                'type': 'list',
                'name': 'choix_export_post',
                'message': 'Que souhaitez-vous faire avec les résultats obtenus ? ',
                'choices': [
                    'Enregistrer les posts dans la base de données'
                    , 'Ne pas enregistrer et revenir au menu principal'
                ]
            }
        ]


    def make_choice(self):
        answers = prompt(self.__questions)
        if answers['choix_export_post'] == 'Enregistrer les posts dans la base de données':
            for publication in self.list_publication:
                PostDao().add_post(self.id_user, publication)
            from view.operations_view import OperationsView
            return OperationsView(self.id_user).make_choice()
        elif answers['choix_export_post'] == 'Ne pas enregistrer et revenir au menu principal':
            from view.operations_view import OperationsView
            return OperationsView(self.id_user).make_choice()

from PyInquirer import prompt
from view.abstract_view import AbstractView

class ListStocksView(AbstractView):

    def __init__(self, id_user):
        self.id_user = id_user
        self.__questions = [
            {
                'type': 'list',
                'name': 'choix_actions',
                'message': 'Voici vos actions stockées. Souhaitez-vous:',
                'choices': [
                    'Ajouter une action'
                    , 'Supprimer une action'
                    , 'Revenir au menu principal'
                ]
            }
        ]


    def make_choice(self):
        answers = prompt(self.__questions)
        if answers['choix_actions'] == 'Ajouter une action':
            from view.add_stock_view import AddStockView
            return AddStockView(self.id_user).make_choice()
        elif answers['choix_actions'] == 'Supprimer une action':
            from view.remove_stock_view import RemoveStockView
            return RemoveStockView(self.id_user).make_choice()
        elif answers['choix_actions'] == 'Revenir au menu principal':
            from view.operations_view import OperationsView
            return OperationsView(self.id_user).make_choice()
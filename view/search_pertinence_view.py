from PyInquirer import prompt
from view.abstract_view import AbstractView



class SearchPertinenceView(AbstractView):
    def __init__(self, id_user, list_publi_period):
        self.id_user = id_user
        self.list_publi_period = list_publi_period
        self.__questions = [
            {
                'type': 'list',
                'name': 'choix_recherche',
                'message': 'Veuillez choisir entre ces deux options : ',
                'choices': [
                    'Rechercher les X posts les plus commentés'
                    , 'Rechercher les X posts avec le plus de réactions'
                ]
            }
        ]

    def make_choice(self):
        answers = prompt(self.__questions)
        if answers['choix_recherche'] == 'Rechercher les X posts les plus commentés':
            from view.number_posts_comment_view import NumberPostsCommentView
            return NumberPostsCommentView(self.id_user, self.list_publi_period).make_choice()
        elif answers['choix_recherche'] == 'Rechercher les X posts avec le plus de réactions':
            from view.number_posts_reactions_view import NumberbPostsReactionsView
            return NumberbPostsReactionsView(self.id_user, self.list_publi_period).make_choice()

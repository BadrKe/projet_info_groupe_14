from PyInquirer import Separator, prompt
from view.abstract_view import AbstractView
from dao.stock_dao import StockDao


class ExportTermsView(AbstractView):
    def __init__(self, id_user, list_terms):
        self.id_user = id_user
        self.list_terms = list_terms
        self.__questions = [
            {
                'type': 'list',
                'name': 'choix_export_termes',
                'message': 'Que souhaitez-vous faire avec les résultats obtenus ? ',
                'choices': [
                    'Enregistrer les termes dans la base de données'
                    , 'Ne pas enregistrer et revenir au menu principal'
                ]
            }
        ]



    def make_choice(self):
        answers = prompt(self.__questions)
        if answers['choix_export_termes'] == 'Enregistrer les termes dans la base de données':
            for term in self.list_terms:
                StockDao().add_stock(self.id_user, term[0], term[1])
            from view.operations_view import OperationsView
            return OperationsView(self.id_user).make_choice()
        elif answers['choix_export_termes'] == 'Ne pas enregistrer et revenir au menu principal':
            from view.operations_view import OperationsView
            return OperationsView(self.id_user).make_choice()

import psycopg2
import os
import dotenv

class GestConnexion:

    def __init__(self):
        self.etatconnection=False
        self.conn = None
        self.cur = None

    def DBConnect(self):
        dotenv.load_dotenv(override=True)
        if not self.etatconnection:
            try:
                self.conn = psycopg2.connect(
                    user = os.environ["USER"],
                    password = os.environ["PASSWORD"],
                    host = os.environ["HOST"],
                    port = os.environ["PORT"],
                    database = os.environ["DATABASE"]
                )
                self.cur = self.conn.cursor()
                self.etatconnection = True
            except (Exception, psycopg2.Error) as error :
                print ("Erreur lors de la sélection à partir de la table post", error)
        else:
            print("Déjà connecté(e) à la base de données")


    def DBDisconnect(self):
        if self.etatconnection:
            self.cur.close()
            self.conn.close()
            self.etatconnection = False
        else:
            print("Déjà déconnecté(e) de la base")

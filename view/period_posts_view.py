from metiers.search_post import SearchPost
from view.abstract_view import AbstractView
from PyInquirer import prompt
from metiers.import_api import ImportAPI


class PeriodPostsView(AbstractView):
    def __init__(self, id_user):
        self.id_user = id_user
        self.__questions = [
            {
                'type': 'input',
                'name': 'choix_periode_post',
                'message': 'Veuillez entrer une période en heures : '
            }
        ]

    def make_choice(self):
        answers = prompt(self.__questions)
        list_publi_period = SearchPost(answers["choix_periode_post"], ImportAPI().get_api()).search_post_period()
        from view.search_post_view import SearchPostView
        return SearchPostView(self.id_user, list_publi_period).make_choice()


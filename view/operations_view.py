from PyInquirer import prompt
from view.abstract_view import AbstractView
from dao.stock_dao import StockDao

class OperationsView(AbstractView):
    def __init__(self, id_user):
        self.id_user = id_user
        self.__questions = [
            {
                'type': 'list',
                'name': 'choix_op',
                'message': 'Veuillez choisir entre ces différentes options : ',
                'choices': [
                    'Effectuer une recherche sur r/wallstreetbets sur une période'
                    , "Consulter la liste d'actions boursières stockées"
                    , "Sortir un rapport sur l'activité de r/wallstreetbets sur une période"
                    , "Consulter les posts d'un utilisateur de Reddit"
                    , "Quitter l'application"
                ]
            }
        ]



    def make_choice(self):
        answers = prompt(self.__questions)
        if answers['choix_op'] == 'Effectuer une recherche sur r/wallstreetbets sur une période':
            from view.search_view import SearchView
            return SearchView(self.id_user).make_choice()
        elif answers['choix_op'] == "Consulter la liste d'actions boursières stockées":
            list_stocks = StockDao().find_stock_by_id_user(self.id_user)
            for stock in list_stocks:
                print(stock[2], " Nombre d'occurrences: ", stock[4])
            from view.list_stocks_view import ListStocksView
            return ListStocksView(self.id_user).make_choice()
        elif answers['choix_op'] == "Sortir un rapport sur l'activité de r/wallstreetbets sur une période": 
            from view.report_period_view import ReportPeriodView
            return ReportPeriodView(self.id_user).make_choice()
        elif answers['choix_op'] == "Consulter les posts d'un utilisateur de Reddit":
            from view.user_posts_view import UserPostsView
            return UserPostsView(self.id_user).make_choice()
        elif answers['choix_op'] == "Quitter l'application":
            exit()
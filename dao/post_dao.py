import psycopg2
from dao.gest_connexion import GestConnexion
import datetime

class PostDao(GestConnexion):
    
    def find_all(self):
        if not self.etatconnection:
            self.DBConnect()
        sql = "SELECT * FROM post "
        self.cur.execute(sql)
        res = self.cur.fetchall()
        return(res)
        
    def find_by_id(self,id_post):
        if not self.etatconnection:
            self.DBConnect()
        sql = "SELECT * FROM post WHERE id_post="+str(id_post)
        self.cur.execute(sql)
        res = self.cur.fetchall()
        return(res)

    def find_post_by_id_user_and_period(self, id_user, report_period):
        if not self.etatconnection:
            self.DBConnect()
        sql = "SELECT * FROM post WHERE id_utilisateur={} AND current_date - date_ajout<{}".format(str(id_user), str(report_period))
        self.cur.execute(sql)
        res = self.cur.fetchall()
        return(res)

    def add_post(self, id_user, post):
        
        if not self.etatconnection:
            self.DBConnect()

        sql=""" INSERT INTO post (nom_auteur,titre,date_post,contenu,
        nb_reactions,id_utilisateur,nb_commentaires,date_ajout,subreddit) 
        VALUES (%s,%s,%s,%s,%s,%s,%s, current_date,%s) """
        values=(post.author,post.title,post.date,post.selftext,post.nb_reactions,id_user,post.nb_comments,post.subreddit)
        self.cur.execute(sql,values)
        self.conn.commit()  
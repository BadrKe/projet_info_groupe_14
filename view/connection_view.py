from PyInquirer import  prompt
from view.abstract_view import AbstractView
from metiers.connection import Connection
from dao.user_dao import UserDao

class ConnectionView(AbstractView):
    def __init__(self) -> None:
        self.__questions = [
            
            {
                'type': 'input',
                'name': 'pseudo',
                'message': 'Entrez votre pseudo' 
            },
            {
                'type': 'password',
                'name': 'password',
                'message': 'Entrez votre mot de passe'
            }
        ] 


    def make_choice(self):
        answers = prompt(self.__questions)
        Connection().connection(answers["pseudo"], Connection().hash_password(answers["pseudo"], answers["password"]))
        name_user = answers['pseudo']
        id_user = UserDao().find_by_username(name_user)[0][0]
        from view.operations_view import OperationsView
        return OperationsView(id_user).make_choice()
    
    
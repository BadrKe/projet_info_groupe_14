from PyInquirer import prompt
from metiers.search_by_word import SearchByWord
from view.abstract_view import AbstractView
import metiers.import_api as ia

class SearchPostView(AbstractView):
    def __init__(self, id_user, list_publi_period):
        self.id_user = id_user
        self.list_publi_period = list_publi_period
        self.__questions = [
            {
                'type': 'list',
                'name': 'choix_recherche',
                'message': 'Veuillez choisir entre ces quatres options : ',
                'choices': [
                    'Rechercher selon un mot clé saisi' 
                    , "Rechercher selon la liste d'actions stockées"
                    , 'Rechercher selon la pertinence' 
                    , 'Rechercher selon un utilisateur reddit'

                ]
            }
        ]


    def make_choice(self):
        answers = prompt(self.__questions)
        if answers['choix_recherche'] == 'Rechercher selon un mot clé saisi':
            from view.search_post_with_keyword_view import SearchPostWithKeywordView
            return SearchPostWithKeywordView(self.id_user, self.list_publi_period).make_choice()

        elif answers['choix_recherche'] == "Rechercher selon la liste d'actions stockées":
            list_publi_final = SearchByWord(self.list_publi_period).search_publication_with_list_of_words(self.id_user)
            print(ia.ImportAPI().formatting(list_publi_final))
            from view.export_post_view import ExportPostView 
            return ExportPostView(self.id_user, list_publi_final).make_choice()

        elif answers['choix_recherche'] == 'Rechercher selon la pertinence':
            from view.search_pertinence_view import SearchPertinenceView
            return SearchPertinenceView(self.id_user, self.list_publi_period).make_choice()

        elif answers['choix_recherche'] =='Rechercher selon un utilisateur reddit':
            from view.select_user_view import SelectUserView
            return SelectUserView(self.id_user, self.list_publi_period).make_choice()
from dao.stock_dao import StockDao
from dao.post_dao import PostDao
import csv


class ExportReport:
    def __init__(self, id_user, report_period):
        self.id_user = id_user
        self.report_period = report_period

    def export_report(self):
        stocks = StockDao().find_stock_by_id_user_and_period(self.id_user, self.report_period)
        posts = PostDao().find_post_by_id_user_and_period(self.id_user, self.report_period)
        with open('rapport.csv', 'w', newline='') as report:
            writer = csv.writer(report, delimiter=';')
            writer.writerow(["TABLEAU CONCERNANT LE RECAPITULATIF DES POSTS ENREGISTRES CES {} DERNIERS JOURS".format(self.report_period)])
            writer.writerow(" ")
            writer.writerow(["Nom de l'auteur",'Titre','Date du post','Contenu',
        'Nombre de reactions','Nombre de commentaires',"Date d'ajout",'Subreddit'])
            for post in posts:
                writer.writerow((post[1], post[2], post[3], post[4], post[5], post[7], post[8], post[9]))
            for i in range(5):
                writer.writerow(" ")
            writer.writerow(["TABLEAU CONCERNANT LE RECAPITULATIF DES TERMES ENREGISTRES CES {} DERNIERS JOURS".format(self.report_period)])
            writer.writerow(" ")
            writer.writerow(["Nom de l'action boursiere", "Date d'ajout", 'Occurrence'])
            for stock in stocks:
                writer.writerow((stock[2], stock[3], stock[4]))



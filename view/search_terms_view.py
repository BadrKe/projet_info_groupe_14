from PyInquirer import prompt
from view.abstract_view import AbstractView
from metiers.search_terms import SearchTerms

class SearchTermsView(AbstractView):
    def __init__(self, id_user, list_publi_period):
        self.id_user = id_user
        self.list_publi_period = list_publi_period
        self.__questions = [
            {
                'type': 'input',
                'name': 'nb_termes_choisi',
                'message': 'Veuillez entrer le nombre de termes les plus fréquents que vous souhaitez obtenir : '
            }
        ]


    def make_choice(self):
        answers = prompt(self.__questions)
        terms = SearchTerms(self.list_publi_period,int(answers['nb_termes_choisi'])).construction_list_posts()
        for term in terms:
            print(term)
        from view.export_terms_view import ExportTermsView
        return ExportTermsView(self.id_user, terms).make_choice()



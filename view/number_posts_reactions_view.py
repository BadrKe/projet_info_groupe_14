from PyInquirer import prompt
from view.abstract_view import AbstractView
from metiers.search_by_pertinence import SearchByPertinence
import metiers.import_api as ia

class NumberbPostsReactionsView(AbstractView):
    def __init__(self, id_user, list_publi_period):
        self.id_user = id_user
        self.list_publi_period = list_publi_period
        self.__questions = [
            {
                'type': 'input',
                'name': 'nb_posts_react',
                'message': 'Veuillez entrer le "X" '
            }
        ]



    def make_choice(self):
        answers = prompt(self.__questions)
        list_publication_final = SearchByPertinence(list_publication = self.list_publi_period).pertinence_reaction(int(answers["nb_posts_react"]))
        print(ia.ImportAPI().formatting(list_publication_final))
        from view.export_post_view import ExportPostView
        return ExportPostView(self.id_user, list_publication_final).make_choice()
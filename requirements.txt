python-dotenv==0.19.1
pandas==1.3.4
PyInquirer==1.0.3
requests==2.26.0
psycopg2==2.9.1
datetime==4.3
python-csv==0.0.13
coverage==6.1.2



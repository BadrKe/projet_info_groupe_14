import metiers.search_post as rp
import metiers.import_api as ia
import operator, re


class SearchTerms:
    
    def __init__(self,list_publication, number_terms):
        self.list_publication = list_publication
        self.number_terms = number_terms

    def construction_list_posts(self):
        # Creation of the list of words which have not to be consider in the search
        forbidden_words = [ '-', ';', ':', '!', '?', '...', '.','/', '|', '•','#','eu','us','yolo','q1','q2','q3','q4',
            'a','about','above','after','again','against','all','am','an','and','any','are','aren\'t','as','at','be',
            'because','been','before','being','below','between','billion','both','but','by','can\'t','cannot','could','couldn\'t',
            'did','didn\'t','do','does','doesn\'t','doing','don', 'doesn\'','don\'t','down','during','each','few','for','from','further',
            'had','hadn\'t','has','hasn\'t','have','haven\'t','having','he','he\'d','he\'ll','he\'s','her','here','here\'s',
            'hers','herself','him','himself','his','how','how\'s','i','i\'d','i\'ll','i\'m','i\'ve','if','in','into','is',
            'isn\'t','it','it\'s','its','itself','let\'s','long-ter','long-term','me','more','most','mustn\'t','my','myself','no','nor','not',
            'of','off','on','once','only','or','other','ought','our','ours','ourselves','out','over','own','same','shan\'t',
            'she','she\'d','she\'ll','she\'s','should','shouldn\'t','short-term','short-ter','so','some','such','than','that','that\'s','the',
            'their','theirs','them','themselves','then','there','there\'s','these','they','they\'d','they\'ll','they\'re',
            'they\'ve','this','those','through','to','too','under','until','up','very','was','wasn\'t','we','we\'d','we\'ll',
            'we\'re','we\'ve','were','weren\'t','what','what\'s','when','when\'s','where','where\'s','which','while','who',
            'who\'s','whom','why','why\'s','with','won\'t','would','wouldn\'t','you','you\'d','you\'ll','you\'re','you\'ve',
            'your','yours','yourself','yourselves', 'stocks', 'earn', 'earnings', 'earning', 'value', 'values', 'shit', 'fuck', ''
            ]
        
        dico_terms_occurrence={}
        # creation of the dictionary where will have in key : the more quote words, in value : occurence of the word 
        
        for publication in self.list_publication:
       
            list_publi = []
            list_publi.append(publication.title)
            list_publi.append(publication.selftext)
        

            #splitting of the string in several strings
            list_terms = []
            for string in list_publi:  
                list_terms += re.split(r', |; |\. |\s',string)

               
            # for each terms in this list, search of stocks
            # considering terms in capital letters or not entirely composed of letters, 
            # or present in the stock list 
            # list of characters excluding the strings selected in the results : 
            # punctuation characters already processed in list_terms
            
            banished_characters=["\n", "\'", "!", "^", "(", ")", "{", "}", "[", "]", "*", ";", ":", "&", ".", "_", "|", "?", "#", "/", "$"]

            for term_temp in list_terms:
                term=""
                for i in range(0,len(term_temp)):
                    if term_temp[i] not in banished_characters:
                        term+=term_temp[i]    
                if not term.lower() in forbidden_words and not term.isnumeric() and (term.isupper() or not term.isalpha()):
                    
                    ######################## modification test_3_letters #######################
                    
                    number_letters = 0
                    char_selection_test = True

                    for i in range(0,len(term)):
                        if term[i].isalpha():
                            number_letters += 1
                        elif term[i] in banished_characters:
                            char_selection_test = False
                            break

                    if number_letters >= 3 and char_selection_test:
       
                        if term not in dico_terms_occurrence:
                            dico_terms_occurrence[term]=1
                        elif term in dico_terms_occurrence:
                            dico_terms_occurrence[term]+=1

                    ##################end of modification#########################################

    # creation of a sorted list, descending order from the terms_occurrence dictionary
    # result = list_final
        
        list_terms_occurrence_sorted = sorted(dico_terms_occurrence.items(), key=operator.itemgetter(1), reverse = True)
        list_final=[]

        for i in range(0,min(self.number_terms,len(list_terms_occurrence_sorted))):

            list_final.append(list_terms_occurrence_sorted[i])

        return(list_final)



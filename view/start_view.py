from PyInquirer import prompt
from view.abstract_view import AbstractView


class StartView(AbstractView):

    def __init__(self):
        self.__questions = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Bienvenue',
                'choices': [
                    'Créer un compte'
                    , 'Se connecter'
                    , "Quitter l'application"

                ]
            }
        ]


    def make_choice(self):
        answers = prompt(self.__questions)
        if answers['choix'] == 'Créer un compte':
            from view.register_view import RegisterView
            return RegisterView().make_choice()
        elif answers['choix'] == 'Se connecter':
            from view.connection_view import ConnectionView
            return ConnectionView().make_choice()
        elif answers['choix'] == "Quitter l'application":
            pass


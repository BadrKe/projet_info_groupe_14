from PyInquirer import  prompt
from view.abstract_view import AbstractView
from dao.stock_dao import StockDao


class AddStockView(AbstractView):
    def __init__(self, id_user):
        self.id_user = id_user
        self.__questions = [
    
            {
                'type': 'input',
                'name': 'action',
                'message': 'Entrez une action à ajouter à la liste' 
            }
        ]


    def make_choice(self):
        answers = prompt(self.__questions)
        StockDao().add_stock(self.id_user, answers['action'])
        from view.operations_view import OperationsView
        return OperationsView(self.id_user).make_choice()

        

import dao.stock_dao as abd


class SearchByWord:
    def __init__(self, list_publication):
        self.list_publication = list_publication

    def search_publication_with_word(self, keyword):
        list_publication_final = []
        for publication in self.list_publication:
            if keyword.upper() in (publication.title).upper() or keyword.upper() in (publication.selftext).upper():
                list_publication_final.append(publication)
        return list_publication_final


    def search_publication_with_list_of_words(self,id_user):
        list_publication_final = []
        dict_occurrences = {}
        for tuple_stock in abd.StockDao().find_stock_by_id_user(id_user):
            dict_occurrences[tuple_stock[2]]=0
            for publication in self.list_publication:
                if tuple_stock[2].upper() in (publication.title).upper() or tuple_stock[2].upper() in (publication.selftext).upper():
                    list_publication_final.append(publication)
                    dict_occurrences[tuple_stock[2]]+=1
        for stock in dict_occurrences:
            abd.StockDao().update_occurrence(id_user,stock,dict_occurrences[stock]) 
        return list(set(list_publication_final)) #removes duplicates






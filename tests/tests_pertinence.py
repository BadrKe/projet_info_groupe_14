import unittest
import metiers.publication as pub
import metiers.search_by_pertinence as sbp
import datetime

class TestsPertinence(unittest.TestCase):
    
    date = datetime.datetime.strptime('2021-11-11 12:34:56', '%Y-%m-%d %H:%M:%S') 
    liste_publis = [
    pub.Publication('title1', 'selftext1', date, 'author1', nb_comments=10, nb_reactions=82, subreddit='wallstreetbets'),
    pub.Publication('title2', 'selftext2', date, 'author2', nb_comments=19, nb_reactions=194, subreddit='wallstreetbets'),
    pub.Publication('title3', 'selftext3', date, 'author3', nb_comments=35, nb_reactions=39, subreddit='wallstreetbets'),
    pub.Publication('title4', 'selftext4', date, 'author4', nb_comments=12, nb_reactions=77, subreddit='wallstreetbets'),
    pub.Publication('title5', 'selftext5', date, 'author5', nb_comments=52, nb_reactions=41, subreddit='wallstreetbets'),
    pub.Publication('title6', 'selftext6', date, 'author6', nb_comments=25, nb_reactions=132, subreddit='wallstreetbets')]
    number_posts=3

    most_commented = sbp.SearchByPertinence(liste_publis).pertinence_comments(number_posts)
    most_reacted_to = sbp.SearchByPertinence(liste_publis).pertinence_reaction(number_posts)


    def test_pertinence_comments(self):
        self.assertEqual(list(map(lambda x:x.title, self.most_commented)), ['title5', 'title3', 'title6'])

    def test_pertinence_reaction(self):
        self.assertEqual(list(map(lambda x:x.title,self.most_reacted_to)), ['title2', 'title6', 'title1'])


#pour lancer test: python3.9 -m unittest'
#pour vérifier coverage : 'python3.9 -m coverage run tests/tests_pertinence.py' puis 'python3.9 -m coverage report'

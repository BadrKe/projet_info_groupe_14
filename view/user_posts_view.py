from PyInquirer import prompt
from view.abstract_view import AbstractView
from metiers.search_post import SearchPost
import metiers.import_api as ia


class UserPostsView(AbstractView):
    def __init__(self, id_user):
        self.id_user = id_user
        self.__questions = [
            {
                'type': 'input',
                'name': 'auteur',
                'message': "Veuillez entrer l'utilisateur reddit dont vous souhaitez voir les posts:"
            },
            {
                'type': 'input',
                'name': 'periode',
                'message': "Veuillez entrer une période (en heures) sur laquelle récupérer des posts:"
            }
        ]

    def make_choice(self):
        answers = prompt(self.__questions)
        list_posts = SearchPost(answers['periode'],ia.ImportAPI().get_user_posts(answers['auteur'])).search_post_period()
        print(ia.ImportAPI().formatting(list_posts))
        from view.export_post_view import ExportPostView
        return ExportPostView(self.id_user, list_posts).make_choice()
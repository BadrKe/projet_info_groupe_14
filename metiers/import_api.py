import requests
import os
import dotenv
import datetime
from metiers.publication import Publication
import pandas as pd

class ImportAPI:
    def __init__(self):
        pass

    def generate_token(self):
        dotenv.load_dotenv(override=True)
        auth = requests.auth.HTTPBasicAuth(os.environ["USER_ID"], os.environ["SECRET_TOKEN"])

        data = {'grant_type': 'password',
            'username': os.environ["USERNAME_REDDIT"],
            'password': os.environ["PASSWORD_REDDIT"]}

        headers = {'User-Agent': 'MyBot/0.0.1'}

        res = requests.post('https://www.reddit.com/api/v1/access_token',
                    auth=auth, data=data, headers=headers)

        TOKEN = res.json()['access_token']

        headers = {**headers, **{'Authorization': "bearer {}".format(TOKEN)}}

        return headers

    def get_api(self):
        headers = self.generate_token()
        res = requests.get("https://oauth.reddit.com/r/wallstreetbets/new",
                   headers=headers, params={"limit":"100"})
        list_posts = []         
        for post in res.json()["data"]["children"]:
            publication = Publication(title = post["data"]["title"], selftext = post["data"]["selftext"], date =datetime.datetime.fromtimestamp(post["data"]["created"]), author=post["data"]["author"],
            nb_comments=post["data"]["num_comments"], nb_reactions=post["data"]["score"], subreddit="wallstreetbets")
            list_posts.append(publication)
        return list_posts


    def get_user_posts(self, author):
        headers = self.generate_token()
        res = requests.get("https://oauth.reddit.com/user/{}/submitted".format(author),
                   headers=headers, params={"limit":"100"})
        list_posts = []         
        for post in res.json()["data"]["children"]:
            publication = Publication(title=post["data"]["title"], selftext=post["data"]["selftext"], date =datetime.datetime.fromtimestamp(post["data"]["created"]), author=post["data"]["author"],
            nb_comments=post["data"]["num_comments"], nb_reactions=post["data"]["score"], subreddit=post["data"]["subreddit"])
            list_posts.append(publication)
        return list_posts
    

    def formatting(self, list_publication):

        df = pd.DataFrame()

        for publication in list_publication :

            df = df.append(
                 {"Titre : " : publication.title, 
                "Date : " : publication.date, 
                "Auteur : " : publication.author,
                "Nombre de commentaires : " : publication.nb_comments,
                "Nombre de réactions : " : publication.nb_reactions,
                "Subreddit : " : publication.subreddit
                }, ignore_index = True  
            )
        return df


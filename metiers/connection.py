import hashlib
from dao.user_dao import UserDao


class Connection:

    def __init__(self):
        pass

    def hash_password(self, idep, password):
        salt = idep
        mdp_hash = hashlib.sha256(salt.encode() + password.encode()).hexdigest()
        return mdp_hash

    def create_account(self, id, password):
        if UserDao().find_by_username(id)==[]:
            UserDao().add_user(id, password)
        else:
            print('Identifiant déjà utilisé')
            from view.register_view import RegisterView
            return RegisterView().make_choice()

    def connection(self, name, password):
        if UserDao().find_by_username(name)==[]:
            print('Identifiant inconnu')
            from view.connection_view import ConnectionView
            return ConnectionView().make_choice()
        else:
            if password != UserDao().find_by_username(name)[0][3]:
                print('Mot de passe incorrect')
                from view.connection_view import ConnectionView
                return ConnectionView().make_choice()
            else:
                pass



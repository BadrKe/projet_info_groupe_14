from datetime import datetime, timedelta


class SearchPost():


    def __init__(self, period, list_publication ):
        self.list_publication = list_publication
        self.period = period


    def search_post_period(self):
        current_date = datetime.now()
        deadline = current_date - timedelta(hours = int(self.period))
        list = []
        for publication in self.list_publication :
            if publication.date >= deadline :
                list.append(publication)
        return(list)




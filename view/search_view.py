from PyInquirer import prompt
from view.abstract_view import AbstractView


class SearchView(AbstractView):
    def __init__(self, id_user):
        self.id_user = id_user
        self.__questions = [
            {
                'type': 'list',
                'name': 'choix_recherche',
                'message': 'Veuillez choisir entre ces deux options : ',
                'choices': [
                    'Rechercher des posts'
                    , 'Rechercher les X termes les plus fréquents'
                ]
            }
        ]


    def make_choice(self):
        answers = prompt(self.__questions)
        if answers['choix_recherche'] == 'Rechercher des posts':
            from view.period_posts_view import PeriodPostsView
            return PeriodPostsView(self.id_user).make_choice()
        elif answers['choix_recherche'] == 'Rechercher les X termes les plus fréquents':
            from view.period_terms_view import PeriodTermsView
            return PeriodTermsView(self.id_user).make_choice()

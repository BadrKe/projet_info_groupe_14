import psycopg2
import datetime
from dao.gest_connexion import GestConnexion

class StockDao(GestConnexion):

    def find_stock_all(self):
        if not self.etatconnection:
            self.DBConnect()
        sql = "SELECT * FROM action_boursiere"
        self.cur.execute(sql)
        res = self.cur.fetchall()
        return(res)
        

    def find_stock_by_id_user(self,id_user):
        if not self.etatconnection:
            self.DBConnect()
        sql = "SELECT * FROM action_boursiere WHERE id_utilisateur="+str(id_user)
        self.cur.execute(sql)
        res = self.cur.fetchall()
        return(res)

    def find_stock_by_id_user_and_period(self, id_user, report_period):
        if not self.etatconnection:
            self.DBConnect()
        sql = "SELECT * FROM action_boursiere WHERE id_utilisateur={} AND current_date - date_creation_dans_bdr<{}".format(str(id_user), str(report_period))
        self.cur.execute(sql)
        res = self.cur.fetchall()
        return(res)

    def remove_stock(self,id_user,name_action):
        if not self.etatconnection:
                self.DBConnect()
        sql="DELETE FROM action_boursiere WHERE nom_action_boursiere =%s AND id_utilisateur=%s " 
        self.cur.execute(sql, (name_action,str(id_user)))
        self.conn.commit()


    def add_stock(self,id_user,name_action,occurrence=0):
        '''    
        ajout des :
        id_utilisateur type integer : INT : user identifier
        nom_action_boursiere : TEXT : share name
        date_creation_dans_bdr : time : automatic date
        occurrence : INT : occurring number in posts 
        '''
        
        if not self.etatconnection:
            self.DBConnect()
        
        sql_partie_1="INSERT INTO action_boursiere(id_utilisateur, nom_action_boursiere, date_creation_dans_bdr, occurrence) VALUES ("
        sql_partie_2=str(id_user) + ", '" + name_action + "', current_date, " + str(occurrence) + ")"
        sqljoint=sql_partie_1+sql_partie_2
        self.cur.execute(sqljoint)
        self.conn.commit()
        

    def update_occurrence(self,id_user,name_action,occurrence):
        if not self.etatconnection:
            self.DBConnect()
        
        sql="UPDATE action_boursiere SET occurrence=%s,date_creation_dans_bdr=current_date WHERE id_utilisateur=%s and nom_action_boursiere=%s"
        self.cur.execute(sql, (str(occurrence),str(id_user),name_action))
        self.conn.commit()
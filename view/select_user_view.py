from PyInquirer import prompt
from view.abstract_view import AbstractView
from metiers.search_by_author import SearchByAuthor
import metiers.import_api as ia


class SelectUserView(AbstractView):
    def __init__(self, id_user, list_publi_period):
        self.id_user = id_user
        self.list_publi_period = list_publi_period
        self.__questions = [
            {
                'type': 'input',
                'name': 'auteur',
                'message': "Veuillez entrer l'utilisateur reddit dont vous souhaitez voir les posts "
            }
        ]

    def make_choice(self):
        answers = prompt(self.__questions)
        list_publication_final = SearchByAuthor(list_publication = self.list_publi_period).search_publication_author(answers['auteur'])
        print(ia.ImportAPI().formatting(list_publication_final))
        from view.export_post_view import ExportPostView
        return ExportPostView(self.id_user, list_publication_final).make_choice()